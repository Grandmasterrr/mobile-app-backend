FROM node:16.13.1-alpine as builder
WORKDIR /repo
COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build
# Remove dev-specific packages from node_modules
RUN npm prune --production

FROM node:16.13.1-alpine
WORKDIR /app
# Pull built application to /service/dist/app
COPY --from=builder /repo/dist ./dist
# Pull built migrations to /service/dist/migration
COPY --from=builder /repo/node_modules ./node_modules
EXPOSE 80
ENV APP_PORT 80
CMD ["node", "dist/main.js"]
