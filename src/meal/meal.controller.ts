import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { MealService } from './meal.service';

interface MealPriceResponse {
  price: number;
}

interface MealPriceBatchBody {
  names: string[];
}

interface ExtendedMealPrice {
  value: number;
  unit: string;
}

interface MealPriceBatchResponse {
  prices: ExtendedMealPrice[];
}

@Controller('meal')
export class MealController {
  constructor(private readonly mealService: MealService) {}

  @Get('price')
  getMealPrice(@Query('name') mealName: string): MealPriceResponse {
    return {
      price: this.mealService.getMealPrice(mealName),
    };
  }

  @Post('batchPrices')
  batchMealPrices(@Body() request: MealPriceBatchBody): MealPriceBatchResponse {
    return {
      prices: request.names.map((name) => ({
        value: this.mealService.getMealPrice(name),
        unit: this.mealService.defineMealPriceUnit(name),
      })),
    };
  }
}
