import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

@Injectable()
export class MealService {
  public getMealPrice(mealName: string): number {
    const hash = crypto.createHash('sha1').update(mealName).digest('hex');
    return parseInt(hash.substring(0, 4), 16);
  }

  private static mealPriceUnits = ['RUB', 'USD', 'BTC', 'GRN', 'USDT'];

  public defineMealPriceUnit(mealName: string): string {
    return MealService.mealPriceUnits[
      mealName.charCodeAt(0) % MealService.mealPriceUnits.length
    ];
  }
}
