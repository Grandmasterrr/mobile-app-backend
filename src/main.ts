import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

(async () => {
  const app = await NestFactory.create(AppModule);

  try {
    const port = parseInt(process.env.APP_PORT);
    await app.listen(port);
    console.log(`Started server at port ${port}`);
  } catch (err) {
    throw err;
  }
})();
