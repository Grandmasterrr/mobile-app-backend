import { Module } from '@nestjs/common';
import { GoodsModule } from './goods/goods.module';
import { MealModule } from './meal/meal.module';

@Module({
  imports: [GoodsModule, MealModule],
})
export class AppModule {}
