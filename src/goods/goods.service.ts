import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

function isEven(n: number): boolean {
  return n % 2 == 0;
}

function sum(a: number, b: number): number {
  return a + b;
}

@Injectable()
export class GoodsService {
  public getGoodPrice(goodName: string): number {
    const hash = crypto.createHash('sha1').update(goodName).digest('hex');
    return parseInt(hash.substring(0, 4), 16);
  }

  public getGoodAvailability(goodName: string): boolean {
    const checksum = goodName
      .split('')
      .map((char) => char.charCodeAt(0))
      .reduce(sum, 0);

    return isEven(checksum);
  }
}
