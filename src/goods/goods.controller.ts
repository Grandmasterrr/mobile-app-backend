import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
} from '@nestjs/common';
import { GoodsService } from './goods.service';

interface GoodInfo {
  price: number;
  availability: boolean;
}

interface GoodInfoBatchRequest {
  names: string[];
}

interface GoodInfoBatchResponse {
  info: GoodInfo[];
}

@Controller('goods')
export class GoodsController {
  constructor(private readonly goodsService: GoodsService) {}

  @Get('info')
  public getGoodInfo(@Query('name') goodName?: string): GoodInfo {
    if (goodName) {
      return {
        availability: this.goodsService.getGoodAvailability(goodName),
        price: this.goodsService.getGoodPrice(goodName),
      };
    } else {
      throw new BadRequestException();
    }
  }

  @Post('batchInfo')
  public batchGoodsInfo(
    @Body() request: GoodInfoBatchRequest,
  ): GoodInfoBatchResponse {
    return {
      info: request.names.map((name) => ({
        price: this.goodsService.getGoodPrice(name),
        availability: this.goodsService.getGoodAvailability(name),
      })),
    };
  }
}
